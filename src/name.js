const isNameValid = n => typeof n === 'string' &&
  n.length < 128 &&
  n.length > 1;

class Name {
  constructor(value) {
    if (isNameValid(value)) {
      this.value = value;
    } else {
      throw new Exception("Invalid name");
    }
  }
}

module.exports = { Name };