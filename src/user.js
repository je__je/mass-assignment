const { Email } = require('./email');
const { Name } = require('./name');

// User data model
class User {
  role = "user";
  id = auth_user_id();

  constructor(name, email) {
    this.name = new Name(name);
    this.email = new Email(email);
  }
}

// A psudo authenticator 
// returns authenticated user ID
function auth_user_id() {
    return 2;
}

module.exports = { User, auth_user_id };
